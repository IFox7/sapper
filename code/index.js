// Встановлення змінних гри
let fieldSize = 10,
  sectionsAll,
  gameField = document.querySelector(".gameField"),
  sectionClick,
  minesInGame = [],
  mines = [],
  gameOver = false;
// Кнопка перезапуску гри
const restartBtn = document.querySelector(".startBtn");

// Функція очищення ігрового поля для розпочатку нової гри
const clearField = () => {
  gameField.remove();
  location.reload();
  gameOver = false;
  congrats.innerHTML = "";
  congrats.classList.remove("show");
  setup();
};
// Установки для початку гри.
const setup = () => {
  setTimeout(() => {
    gameField.style.animation = "fade 1.8s";
    gameField.style.opacity = "1";
  }, 200);
  if (gameOver) {
    return;
  }
  for (let i = 0; i < fieldSize * fieldSize; i++) {
    sectionClick = `<div class='sectionClick'></div>`;
    gameField.innerHTML += sectionClick;
  }
  sectionsAll = document.querySelectorAll(".sectionClick");
  let x = 0;
  let y = 0;
  sectionsAll.forEach((section) => {
    section.setAttribute("data-coordinate", `${x},${y}`);
    // Наповнюємо масив координатами мін
    mines.push(`${x},${y}`);
    x++;
    if (x >= fieldSize) {
      x = 0;
      y++;
    }
    // Ставимо флаг правою кнопкою миші
    section.oncontextmenu = function (e) {
      e.preventDefault();
      flag(section);
    };
    section.addEventListener("click", (e) => {
      console.log(e);
      if (e.target.className === "flag") {
        return;
      } else openSection(section);
      section.classList.add("sectionPressed");
    });
  });
  // Визначення випадкових координат мін на ігровому полі
  for (let i = 0; i < fieldSize; i++) {
    let randomIndex = Math.floor(Math.random(mines.length) * 100);
    minesInGame.push(mines[randomIndex]);
    minesInGame = [...new Set(minesInGame)];
  }
};
setup();

// Розставляння мін на ігровому полі для початку гри
let sectionArr = document.querySelectorAll(".sectionClick"),
  mineLocationArr = [];
Array.from(sectionArr).forEach((elem) => {
  minesInGame.forEach((el) => {
    if (elem.dataset.coordinate === el) {
      elem.innerHTML = `<img class="visibility" src="../img/mine.png" alt="mine" />`;
      mineLocationArr.push(elem.dataset.coordinate.split(","));
    }
  });
});

// Функція відображення кількості мін поруч з клітиною
let counter = 0;
const mineCounter = () => {
  mineLocationArr.forEach((elem) => {
    let x = parseInt(elem[0]),
      y = parseInt(elem[1]),
      xNext = parseInt(elem[0]) + 1,
      yNext = parseInt(elem[1]) + 1,
      xPrev = parseInt(elem[0]) - 1,
      yPrev = parseInt(elem[1]) - 1;
    Array.from(sectionArr).forEach((el) => {
      let fieldX = parseInt(el.dataset.coordinate.split(",")[0]),
        fieldY = parseInt(el.dataset.coordinate.split(",")[1]);
      if (fieldX === xNext && fieldY === y && !el.children[0]) {
        el.textContent++;
        el.classList.add("blur");
      }
      if (fieldX === xPrev && fieldY === y && !el.children[0]) {
        el.textContent++;
        el.classList.add("blur");
      }
      if (fieldX === x && fieldY === yPrev && !el.children[0]) {
        el.textContent++;
        el.classList.add("blur");
      }
      if (fieldX === x && fieldY === yNext && !el.children[0]) {
        el.textContent++;
        el.classList.add("blur");
      }
      if (fieldX === xNext && fieldY === yPrev && !el.children[0]) {
        el.textContent++;
        el.classList.add("blur");
      }
      if (fieldX === xPrev && fieldY === yPrev && !el.children[0]) {
        el.textContent++;
        el.classList.add("blur");
      }
      if (fieldX === xNext && fieldY === yNext && !el.children[0]) {
        el.textContent++;
        el.classList.add("blur");
      }
      if (fieldX === xPrev && fieldY === yNext && !el.children[0]) {
        el.textContent++;
        el.classList.add("blur");
      }
    });
  });
};
mineCounter();

// Зображення міни при натисканні на клітину з нею
const visibile = (e) => {
  if (e.target.children[0]) {
    e.target.children[0].classList.toggle("visibility");
  }
};
Array.from(sectionsAll).forEach((elem) => {
  elem.addEventListener("click", (e) => visibile(e));
});

// Функція встановлення флага на клітині
let prevHtmlElement = "";
const flag = (section) => {
  if (gameOver) return;
  if (!section.classList.contains("sectionPressed")) {
    if (!section.classList.contains("flagOn")) {
      prevHtmlElement = section.innerHTML;
      section.innerHTML = `<img class="flag" src="../img/flag.png" alt="flag" />`;
      section.classList.add("flagOn");
    } else {
      section.innerHTML = prevHtmlElement;
      section.classList.remove("flagOn");
    }
  }
  setTimeout(() => {
    checkVictory();
  }, 100);
};

// Відкриття секції однакових клітин без мін.Заборона відкриття клітини,позначеною флагом.
const clickOpen = (target) => {
  if (!target.classList.contains("flagOn")) {
    target.classList.add("sectionPressed");
  }
};

// Відкриття секції одноманітних клітин без мін.
const openSection = (section) => {
  let location = section.getAttribute("data-coordinate");
  let position = location.split(",");
  let x = parseInt(position[0]);
  let y = parseInt(position[1]);
  if (section.innerHTML === "") {
    if (x > 0) {
      let targetW = document.querySelector(`[data-coordinate="${x - 1},${y}"]`);
      clickOpen(targetW);
    }
    if (x < fieldSize - 1) {
      let targetE = document.querySelector(`[data-coordinate="${x + 1},${y}"]`);
      clickOpen(targetE);
    }
    if (y > 0) {
      let targetN = document.querySelector(`[data-coordinate="${x},${y - 1}"]`);
      clickOpen(targetN);
    }
    if (y < fieldSize - 1) {
      let targetS = document.querySelector(`[data-coordinate="${x},${y + 1}"]`);
      clickOpen(targetS);
    }
    if (x > 0 && y > 0) {
      let targetNW = document.querySelector(
        `[data-coordinate="${x - 1},${y - 1}"]`
      );
      clickOpen(targetNW);
    }
    if (x < fieldSize - 1 && y < fieldSize - 1) {
      let targetSE = document.querySelector(
        `[data-coordinate="${x + 1},${y + 1}"]`
      );
      clickOpen(targetSE);
    }
    if (y > 0 && x < fieldSize - 1) {
      let targetNE = document.querySelector(
        `[data-coordinate="${x + 1},${y - 1}"]`
      );
      clickOpen(targetNE);
    }
    if (x > 0 && y < fieldSize - 1) {
      let targetSW = document.querySelector(
        `[data-coordinate="${x - 1},${y + 1}"]`
      );
      clickOpen(targetSW);
    }
  } else if (section.children[0]) {
    endGame(section);
  }
};

// Функція подій при потраплянні кліка на поле з міною
const endGame = (section) => {
  gameOver = true;
  if (section.children[0].className === "visibility") {
    setTimeout(() => {
      gameField.style.width = 30 + "vw";
      gameField.style.height = 30 + "vw";
      gameField.innerHTML = `<img class="game_over" src="../img/game-over.png" alt="game_over" />`;
    }, 1000);
  }
};

// Функція висвітлення напису про перемогу гравця
const checkVictory = () => {
  let win = true;
  Array.from(sectionArr).forEach((elem) => {
    elem.childNodes.forEach((el) => {
      if (el.className === "visibility") {
        win = false;
      }
    });
  });
  if (win) {
    gameField.style.width = 30 + "vw";
    gameField.style.height = 30 + "vw";
    gameField.innerHTML = `<img class="winner" src="../img/winner.png" alt="winner" />`;
    gameOver = true;
  }
};

// Очищення результату гри. Перезапуск гри.
restartBtn.addEventListener("click", function (e) {
  e.preventDefault();
  clearField();
});


